//
//  PostTableViewController.swift
//  PostUI
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit

public final class PostViewController: UITableViewController {
    
    @IBOutlet public private(set) weak var bodyTextView: UITextView!
    @IBOutlet public private(set) weak var doneButton: UIButton!
    @IBOutlet public private(set) weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet public private(set) weak var doneBarButtonItem: UIBarButtonItem!
    @IBOutlet public private(set) weak var activityIndicatorView: UIActivityIndicatorView!
}

extension PostViewController {
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        tableView.dataSource = self
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension PostViewController {
    public static func instantiate() -> PostViewController? {
        let bundle = Bundle(for: PostViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        return storyboard.instantiateInitialViewController() as? PostViewController
    }
}
