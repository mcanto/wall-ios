//
//  SignInViewController.swift
//  SignInUI
//
//  Created by Mario Canto on 9/6/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit

public final class SignInViewController: UITableViewController {
    
    @IBOutlet public private(set) weak var emailTextField: UITextField!
    @IBOutlet public private(set) weak var passwordTextField: UITextField!
    @IBOutlet public private(set) weak var submitButton: UIButton!
    @IBOutlet public private(set) weak var useWallButton: UIButton!
    @IBOutlet public private(set) weak var signUpButton: UIButton!
    @IBOutlet public private(set) weak var activityIndicatorView: UIActivityIndicatorView!
}

extension SignInViewController {
    public static func instantiate() -> SignInViewController? {
        let bundle = Bundle(for: SignInViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
                
        return storyboard.instantiateInitialViewController() as? SignInViewController
    }
}
