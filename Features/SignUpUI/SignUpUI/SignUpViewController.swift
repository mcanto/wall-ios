//
//  SignUpViewController.swift
//  SignUpUI
//
//  Created by Mario Canto on 9/6/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit

public final class SignUpViewController: UITableViewController {
    
    @IBOutlet public private(set) weak var nameTextField: UITextField!
    @IBOutlet public private(set) weak var emailTextField: UITextField!
    @IBOutlet public private(set) weak var passwordTextField: UITextField!
    @IBOutlet public private(set) weak var passwordConfirmationTextField: UITextField!
    @IBOutlet public private(set) weak var submitButton: UIButton!
    @IBOutlet public private(set) weak var activityIndicatorView: UIActivityIndicatorView!
    
}

extension SignUpViewController {
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension SignUpViewController {
    public static func instantiate() -> SignUpViewController? {
        let bundle = Bundle(for: SignUpViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        return storyboard.instantiateInitialViewController() as? SignUpViewController
    }
}
