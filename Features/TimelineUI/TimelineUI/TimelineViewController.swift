//
//  TimelineViewController.swift
//  TimelineUI
//
//  Created by Mario Canto on 9/6/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit

public final class TimelineViewController: UIViewController {

    @IBOutlet public private(set) weak var tableView: UITableView!
    @IBOutlet public private(set) weak var postBarButtonItem: UIBarButtonItem!
    @IBOutlet public private(set) var logOutBarButtonItem: UIBarButtonItem!
}

extension TimelineViewController {
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        tableView.dataSource = self
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension TimelineViewController: UITableViewDataSource {
    
    public func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int)
    -> Int {
        
        return 10
    }
    
    public func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath)
    -> UITableViewCell {        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: TimelineTableCell.self),
            for: indexPath
        )
        return cell
    }
}

extension TimelineViewController {
    public static func instantiate() -> TimelineViewController? {
        let bundle = Bundle(for: TimelineViewController.self)
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        return storyboard.instantiateInitialViewController() as? TimelineViewController
    }
}

public final class TimelineTableCell: UITableViewCell {
    
//    @IBOutlet public private(set) weak var titleLabel: UILabel!
    @IBOutlet public private(set) weak var usernameLabel: UILabel!
    @IBOutlet public private(set) weak var bodyLabel: UILabel!
    @IBOutlet public private(set) weak var createdAtLabel: UILabel!
    
}
