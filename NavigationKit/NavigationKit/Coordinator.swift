//
//  Coordinator.swift
//  NavigationKit
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

public protocol BaseCoordinatorType: class {
    associatedtype DeepLinkType
    func start()
    func start(with link: DeepLinkType?)
}

public protocol PresentableCoordinatorType: BaseCoordinatorType, Presentable {}

open class PresentableCoordinator<DeepLinkType>: NSObject, PresentableCoordinatorType {
    
    public override init() {
        super.init()
    }
    
    open func start() { start(with: nil) }
    open func start(with link: DeepLinkType?) {}
    
    open var presentable: UIViewController {
        fatalError("Must override toPresentable()")
    }
    
}


public protocol CoordinatorType: PresentableCoordinatorType {
    var router: RouterProtocol { get }
}


open class Coordinator<DeepLinkType>: PresentableCoordinator<DeepLinkType>, CoordinatorType  {
    
    public var childCoordinators: [Coordinator<DeepLinkType>] = []
    
    open var router: RouterProtocol
    
    public init(router: RouterProtocol) {
        self.router = router
        super.init()
    }
    
    public func addChild(_ coordinator: Coordinator<DeepLinkType>) {
        childCoordinators.append(coordinator)
    }
    
    public func removeChild(_ coordinator: Coordinator<DeepLinkType>?) {
        guard let childCoordinator = coordinator else { return }
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }
    
    public func removeAllChild() {
        self.childCoordinators.removeAll()
    }
    
    
    open override var presentable: UIViewController {
        return router.presentable
    }
}
