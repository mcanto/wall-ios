//
//  ModalPresentable.swift
//  NavigationKit
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

public protocol ModalPresentable {
    func present(_ viewControllerToPresent: Presentable, animated flag: Bool, completion: (() -> Void)?)
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
    func add(_ child: Presentable, frame: CGRect?)
    func remove(_ child: Presentable?)
}
