//
//  Navigable.swift
//  NavigationKit
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

public protocol Navigable {
    var navigationController: UINavigationController { get }
    func push(_ viewController: Presentable, animated: Bool, completion: (() -> Void)?) // Uses a horizontal slide transition. Has no effect if the view controller is already in the stack.
    
    
    func pop(animated: Bool)
    func pop(to viewController: Presentable, animated: Bool)
    func popToRoot(animated: Bool)
    func set(_ viewControllers: [Presentable], animated: Bool)
    func setRoot(_ viewController: Presentable, animated: Bool)
}
