//
//  Presentable.swift
//  NavigationKit
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

public protocol Presentable: AnyObject {
    var presentable: UIViewController { get }
}

extension UIViewController: Presentable {
    public var presentable: UIViewController {
        return self
    }
}
