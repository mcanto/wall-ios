//
//  Router.swift
//  NavigationKit
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

public typealias RouterProtocol = Presentable & Navigable & ModalPresentable

public final class Router: NSObject, UINavigationControllerDelegate {
    
    public var navigationController: UINavigationController
    private var completions: [UIViewController : () -> Void]
    
    var rootViewController: UIViewController {
        return navigationController
    }
    
    public init(navigationController: () -> UINavigationController) {
        self.navigationController = navigationController()
        self.completions = [:]
        super.init()
        self.navigationController.delegate = self
    }
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.completions = [:]
        super.init()
        navigationController.delegate = self
    }
}

extension Router: Presentable {
    public var presentable: UIViewController {
        return rootViewController
    }
}

extension Router: ModalPresentable {
    
    public func present(_ viewControllerToPresent: Presentable, animated flag: Bool, completion: (() -> Void)?) {
        rootViewController.present(viewControllerToPresent.presentable, animated: flag, completion: completion)
    }
    
    public func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        rootViewController.dismiss(animated: flag, completion: completion)
    }
    
    public func add(_ child: Presentable, frame: CGRect?) {
        rootViewController.addChild(child.presentable)
        
        if let frame = frame {
            child.presentable.view.frame = frame
        } else {
            child.presentable.view.frame = rootViewController.view.bounds
            child.presentable.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        rootViewController.view.addSubview(child.presentable.view)
        child.presentable.didMove(toParent: rootViewController)
    }
    
    public func remove(_ child: Presentable?) {
        guard let child = child else { return }
        child.presentable.willMove(toParent: nil)
        child.presentable.view.removeFromSuperview()
        child.presentable.removeFromParent()
    }
}

extension Router: Navigable {
    public func push(_ viewController: Presentable, animated: Bool, completion: (() -> Void)? = nil) {
        guard viewController.presentable is UINavigationController == false else { return }
        if let completion = completion {
            completions[viewController.presentable] = completion
        }
        navigationController.pushViewController(viewController.presentable, animated: animated)
    }
    
    public func pop(animated: Bool) {
        if let controller = navigationController.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    public func pop(to viewController: Presentable, animated: Bool) {
        if let controllers = navigationController.popToViewController(viewController.presentable, animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
    
    public func popToRoot(animated: Bool) {
        if let controllers = navigationController.popToRootViewController(animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
    
    public func set(_ viewControllers: [Presentable], animated: Bool) {
        completions.forEach { $0.value() }
        navigationController.setViewControllers(viewControllers.map({ $0.presentable }), animated: animated)
    }
    
    public func setRoot(_ viewController: Presentable, animated: Bool) {
        completions.forEach { $0.value() }
        navigationController.setViewControllers([viewController].map({ $0.presentable }), animated: animated)
    }
    
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        // Ensure the view controller is popping
        guard let poppedViewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(poppedViewController) else {
            return
        }
        runCompletion(for: poppedViewController)
    }
    
    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else {
            return
        }
        completion()
        completions.removeValue(forKey: controller)
    }
}
