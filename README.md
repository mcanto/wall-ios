Dependencies are managed by [Carthage](https://github.com/Carthage/Carthage)

## Carthage

Carthage is a decentralized dependency manager for Cocoa, currently carthage is managing dependencies for Swift code.

You can use Homebrew and install the carthage tool on your system simply by running

```brew update```
```brew install carthage```

Bootstrap carthage

```carthage bootstrap --platform ios```