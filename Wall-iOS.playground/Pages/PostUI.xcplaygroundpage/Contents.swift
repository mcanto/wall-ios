//: [Previous](@previous)

import UIKit
import PlaygroundSupport
import PostUI


let viewController = PostViewController.instantiate()!
let navigationController = UINavigationController()
navigationController.viewControllers = [viewController]

let (parent, _) = playgroundControllers(
    device: .phone4inch,
    orientation: .portrait,
    child: navigationController
)

PlaygroundPage.current.liveView = parent

//: [Next](@next)
