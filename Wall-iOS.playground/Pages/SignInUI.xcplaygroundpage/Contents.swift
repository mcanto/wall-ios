//: [Previous](@previous)

import UIKit
import PlaygroundSupport
import SignInUI

let viewController = SignInViewController.instantiate()!

let (parent, _) = playgroundControllers(
    device: .phone4inch,
    orientation: .portrait,
    child: viewController
)

PlaygroundPage.current.liveView = parent

//: [Next](@next)
