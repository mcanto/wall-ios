//: [Previous](@previous)
import UIKit
import PlaygroundSupport
import SignUpUI

let viewController = SignUpViewController.instantiate()!

let (parent, _) = playgroundControllers(
    device: .phone4inch,
    orientation: .portrait,
    child: viewController
)

PlaygroundPage.current.liveView = parent
//: [Next](@next)
