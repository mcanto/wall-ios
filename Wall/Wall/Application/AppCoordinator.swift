//
//  AppCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit
import NavigationKit

enum Link<T: Identifiable> {
    case auth
    case signIn
    case signUp
    case timeline
    case post(id: Identifier<T>)
}

final class AppCoordinator: Coordinator<Link<User>> {
    
    private let appService: AppService
    
    init(router: RouterProtocol, appService: AppService) {
        self.appService = appService
        super.init(router: router)
    }
    
    
    override func start(with link: Link<User>?) {
        
        guard let link = link else {
            return
        }
        
        switch link {
        case .auth:
            let authCoordinator = AuthCoordinator(router: router, loadUserHandler: appService.loadUser)
            authCoordinator.start()
            authCoordinator.onSignedOut = { [unowned self, weak authCoordinator] in
                self.removeChild(authCoordinator)
                toSignIn(self, appService: self.appService)
            }
            authCoordinator.onSignedIn = { [unowned self, weak authCoordinator] user in
                self.removeChild(authCoordinator)
                toTimeline(self, appService: self.appService, authToken: user.authToken)
            }
            addChild(authCoordinator)
            router.setRoot(authCoordinator, animated: true)            
        case .signIn:
            break
        case .signUp:
            break
        case .timeline:
            break
        case .post:
            break
        }
        
        
    }
}

private func toCreatePost(_ appCoordinator: AppCoordinator, appService: AppService) {
    let navigationController = UINavigationController()
    let router = Router(navigationController: navigationController)
    let postCoordinator = PostCoordinator(
        router: router,
        createPostHandler: appService.createPost
    )
    postCoordinator.start()
    postCoordinator.onCancel = { [weak postCoordinator] in
        appCoordinator.removeChild(postCoordinator)
        appCoordinator.router.dismiss(animated: true, completion: nil)
    }
    postCoordinator.onFinished = { [weak postCoordinator] in
        appCoordinator.removeChild(postCoordinator)
        appCoordinator.router.dismiss(animated: true, completion: nil)        
    }
    appCoordinator.addChild(postCoordinator)
    appCoordinator.router.present(postCoordinator, animated: true, completion: nil)
}

private func toTimeline(_ appCoordinator: AppCoordinator, appService: AppService, authToken: String?) {
    let timelineCoordinator = TimelineCoordinator(
        router: appCoordinator.router,
        authToken: authToken,
        getPostsHandler: appService.getPosts
    )
    timelineCoordinator.start()
    timelineCoordinator.onCreatePost = {
        toCreatePost(appCoordinator, appService: appService)
    }
    timelineCoordinator.onSignIn = { [weak timelineCoordinator]  in
        appCoordinator.removeChild(timelineCoordinator)
        toSignIn(appCoordinator, appService: appService)
    }
    timelineCoordinator.onPostSelection = { post in
        let detailCoordinator = DetailCoordinator(router: appCoordinator.router, post: post)
        detailCoordinator.onDone = { [weak detailCoordinator] in
            appCoordinator.removeChild(detailCoordinator)
            appCoordinator.router.pop(animated: true)
        }
        detailCoordinator.start()
        appCoordinator.addChild(detailCoordinator)
        appCoordinator.router.push(detailCoordinator, animated: true) { [weak detailCoordinator] in
            appCoordinator.removeChild(detailCoordinator)
        }
    }
    timelineCoordinator.onLogOut = { [weak timelineCoordinator]  in
        SessionStorage.removeAuth()
        appCoordinator.removeChild(timelineCoordinator)
        toSignIn(appCoordinator, appService: appService)
    }
    appCoordinator.addChild(timelineCoordinator)
    appCoordinator.router.setRoot(timelineCoordinator, animated: true)
}

private func toSignIn(_ appCoordinator: AppCoordinator, appService: AppService) {
    let signInCoordinator = SignInCoordinator(router: appCoordinator.router, signInHandler: appService.signIn)
    signInCoordinator.start()
    signInCoordinator.onHoldSigIn = { [weak signInCoordinator] in
        appCoordinator.removeChild(signInCoordinator)
        toTimeline(appCoordinator, appService: appService, authToken: nil)
    }
    signInCoordinator.onSignUp = { [weak signInCoordinator] in
        let signUpCoordinator = SignUpCoordinator(router: appCoordinator.router, signUpHandler: appService.signUp)
        signUpCoordinator.start()
        signUpCoordinator.onSignedUp = { [weak signUpCoordinator] auth in
            appCoordinator.removeChild(signUpCoordinator)
            appCoordinator.removeChild(signInCoordinator)
            toTimeline(appCoordinator, appService: appService, authToken: auth.authToken)
        }
        appCoordinator.addChild(signUpCoordinator)
        appCoordinator.router.push(signUpCoordinator, animated: true) { [weak signUpCoordinator] in
            appCoordinator.removeChild(signUpCoordinator)
        }
    }
    signInCoordinator.onSignedIn = { [weak signInCoordinator] user in
        appCoordinator.removeChild(signInCoordinator)
        toTimeline(appCoordinator, appService: appService, authToken: user.authToken)
    }
    appCoordinator.addChild(signInCoordinator)
    appCoordinator.router.setRoot(signInCoordinator, animated: true)
}
