//
//  AppDelegate.swift
//  Wall
//
//  Created by Mario Canto on 9/6/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import UIKit
import NavigationKit
import AuthUI

typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var coordinator: Coordinator<Link<User>>?
//    private let appService = MockAppService()
    private let appService = DefaulAppService()

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: LaunchOptions?)
    -> Bool {                
        
        let navigationController = UINavigationController()
        let router = Router(navigationController: navigationController)
        coordinator = AppCoordinator(router: router, appService: appService)
        coordinator?.start(with: .auth)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}

