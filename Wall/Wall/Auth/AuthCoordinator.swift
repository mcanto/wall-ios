//
//  AuthCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import AuthUI
import RxSwift


final class AuthCoordinator: Coordinator<Link<User>> {
    
    private let viewController: AuthViewController = AuthViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    var onSignedIn: ((Authentication) -> Void)?
    var onSignedOut:  (() -> Void)?
    private let loadUserHandler: LoadUserHandler
    
    deinit {
        print("AUTH deinit")
    }
    
    init(
        router: RouterProtocol,
        loadUserHandler: @escaping LoadUserHandler
    ) {
        self.loadUserHandler = loadUserHandler
        super.init(router: router)
    }
    
    override func start(with link: Link<User>?) {
        
        bindAuthViewController(self.viewController, loadSessionHandler: loadUserHandler)
            .debug("Auth", trimOutput: false)
            .do(onNext: { [unowned self] (state) in
                switch state {
                case .signedOut: self.onSignedOut?()
                case .signedIn(let user): self.onSignedIn?(user)
                case .loading: break
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
    
    
    override var presentable: UIViewController {
        return viewController
    }
}
