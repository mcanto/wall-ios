//
//  AuthView+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import AuthUI

typealias AuthViewFeedback = (ObservableSchedulerContext<AuthView.State>) -> Observable<AuthView.Event>
typealias LoadUserHandler = () -> Single<Authentication>

func bindAuthViewController(
    _ viewController: AuthViewController,
    loadSessionHandler: @escaping LoadUserHandler)
-> Observable<AuthView.State> {
    
    var uiBindings: AuthViewFeedback {
        
        return bind(viewController) { (me, state) -> Bindings<AuthView.Event> in
            
            let subscriptions: [Disposable] = [
                
            ]
            
            let mutations: [Observable<AuthView.Event>] = [
                viewController.rx.viewDidAppear.map({ _ in AuthView.Event.load })
            ]
            return Bindings(subscriptions: subscriptions, events: mutations)
            
        }
    }
    
    
    let loadSessionFeedback: AuthViewFeedback = react(request: { $0.isLoading }) { _ -> Observable<AuthView.Event> in
        return loadSessionHandler()
            .map(AuthView.Event.signIn)
            .catchError({ _ in Single.just(AuthView.Event.signOut) })
            .asObservable()
    }
    
    
    return Observable<Any>.system(
        initialState: AuthView.State.loading,
        reduce: AuthView.reduce,
        scheduler: MainScheduler.instance,
        feedback: uiBindings, loadSessionFeedback
    ).share()
    
}
