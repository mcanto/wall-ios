//
//  AuthView+State.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct LoadUserRequest: Equatable {}


struct AuthView {
    enum State {
        case signedOut
        case signedIn(Authentication)
        case loading
        
        var isLoading: LoadUserRequest? {
            switch self {
            case .loading:
                return LoadUserRequest()
            default:
                return nil
            }
        }
    }
    enum Event {
        case load
        case signIn(Authentication)
        case signOut
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .load:
            return .loading
        case .signIn(let user):
            return .signedIn(user)
        case .signOut:
            return .signedOut
        }
    }
}



