//
//  Detail+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/13/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import PostUI

typealias DetailViewFeedback = (ObservableSchedulerContext<DetailView.State>) -> Observable<DetailView.Event>

func bindDetailViewController(
    _ viewController: PostViewController,
    post: Post)
-> Observable<DetailView.State> {
        
        var uiBindings: DetailViewFeedback {
            
            return bind(viewController) { (me, state) -> Bindings<DetailView.Event> in
                
                let subscriptions: [Disposable] = [
                    state.map({ $0.title }).bind(to: me.navigationItem.rx.title),
                    state.map({ $0.post.body }).bind(to: viewController.bodyTextView.rx.text),
                ]
                let mutations: [Observable<DetailView.Event>] = [
                    me.doneButton.rx.tap.throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in DetailView.Event.done })
                ]
                return Bindings(subscriptions: subscriptions, events: mutations)
                
            }
        }
    
        return Observable<Any>.system(
            initialState: DetailView.State(title: "Post Detail", post: post),
            reduce: DetailView.reduce,
            scheduler: MainScheduler.instance,
            feedback: uiBindings
        ).share()
        
}
