//
//  Detail+State.swift
//  Wall
//
//  Created by Mario Canto on 9/13/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct DetailView {
    struct State: Mutable {
        var title: String
        var post: Post
        var done: Bool
        
        init(title: String, post: Post, done: Bool = false) {
            self.title = title
            self.post = post
            self.done = done
        }
        
    }
    enum Event {
        case none
        case done
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .none:
            return state
        case .done:
            return state.mutate { $0.done = true }
        }
    }
}


