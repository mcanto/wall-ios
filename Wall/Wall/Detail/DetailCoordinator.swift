//
//  DetailCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/13/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import PostUI
import RxSwift


final class DetailCoordinator: Coordinator<Link<User>> {
    
    private let viewController: PostViewController = PostViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    private let post: Post
    var onDone: (() -> Void)?
    
    deinit {
        print("DEINIT DETAIL")
    }
    
    init(
        router: RouterProtocol,
        post: Post
    ) {
        self.post = post
        super.init(router: router)
    }
    
    
    override var presentable: UIViewController {
        return viewController
    }
    
    override func start(with link: Link<User>?) {        
        viewController.navigationItem.leftBarButtonItems = nil
        viewController.navigationItem.rightBarButtonItem = nil
        viewController.loadViewIfNeeded()
        
        bindDetailViewController(viewController, post: post)
            .do(onNext: { [unowned self] state in
                if state.done {
                    self.onDone?()
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
}
