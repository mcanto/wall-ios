//
//  Authentication.swift
//  Wall
//
//  Created by Mario Canto on 9/11/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct Authentication: Codable {
    let authToken: String
    
    enum CodingKeys: String, CodingKey {
        case authToken = "auth_token"
    }
}
