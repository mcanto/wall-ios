//
//  Author.swift
//  Wall
//
//  Created by Mario Canto on 9/13/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct Author: Codable, Hashable {
    let id: Int
    let name: String
    let email: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
    }
}
