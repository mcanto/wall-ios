//
//  Post.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//
import Foundation

struct Post: Decodable {
    var id: Int
    var title: String?
    var body: String
    var createdAt: Date
    var updatedAt: Date?
    let author: Author
    
    enum CodingKeys: String, CodingKey {
        case id
        case body = "content"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case author = "author"
    }
    
    init(id: Int, title: String = "", body: String = "", createdAt: Date = Date(), updatedAt: Date = Date(), author: Author) {
        self.id = id
        self.title = title
        self.body = body
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.author = author
    }
    
}

