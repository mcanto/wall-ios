//
//  User.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct User: Identifiable, Codable {
    typealias RawIdentifier = Int
    
    let id: Identifier<User>
}
