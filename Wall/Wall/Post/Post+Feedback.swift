//
//  Post+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import PostUI

typealias PostViewFeedback = (ObservableSchedulerContext<PostView.State>) -> Observable<PostView.Event>
typealias CreatePostHandler = (String) -> Single<Post>

func bindPostViewController(
    _ viewController: PostViewController,
    post: Post?,
    createPostHandler: @escaping CreatePostHandler)
-> Observable<PostView.State> {
        
        var uiBindings: PostViewFeedback {
            
            return bind(viewController) { (me, state) -> Bindings<PostView.Event> in
                
                let subscriptions: [Disposable] = [
                    state.map({ $0.creatingPost }).bind(to: me.activityIndicatorView.rx.isAnimating),
                    state.map({ !$0.creatingPost }).bind(to: me.bodyTextView.rx.isUserInteractionEnabled),
                    state.map({ !$0.creatingPost }).bind(to: me.doneButton.rx.isEnabled),
                    state.map({ !$0.creatingPost }).bind(to: me.cancelBarButtonItem.rx.isEnabled),
                    state.map({ !$0.creatingPost }).bind(to: me.doneBarButtonItem.rx.isEnabled),
                    state.map({ $0.body }).bind(to: viewController.bodyTextView.rx.text),
                ]
                
                let mutations: [Observable<PostView.Event>] = [
                    viewController.bodyTextView.rx.text.orEmpty.map(PostView.Event.bodyDidChange),
                    viewController.doneButton.rx.tap
                        .throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.init())
                        .map({ _ in PostView.Event.createPost }),
                    viewController.doneBarButtonItem.rx.tap
                        .throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.init())
                        .map({ _ in PostView.Event.createPost }),
                    viewController.cancelBarButtonItem.rx.tap
                        .throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.init())
                        .map({ _ in PostView.Event.cancel }),
                    viewController.rx.viewDidAppear.map({ _ in PostView.Event.viewDidAppear })
                ]
                return Bindings(subscriptions: subscriptions, events: mutations)
                
            }
        }
        
        
        let createPostFeedback: PostViewFeedback = react(request: { $0.createPost }) { request -> Observable<PostView.Event> in
            return createPostHandler(request.body)
                .map({ _ in PostView.Event.didCreatePost })
                .catchError({  Single.just(PostView.Event.didFail($0.localizedDescription)) })
                .asObservable()
        }
        
        
        return Observable<Any>.system(
            initialState: PostView.State.initial(post: post),
            reduce: PostView.reduce,
            scheduler: MainScheduler.instance,
            feedback: uiBindings, createPostFeedback
        ).share()
        
}
