//
//  Post+State.swift
//  Wall
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation

struct CreatePostRequest: Equatable {
    var title: String
    var body: String
}

struct PostView {
    struct State: Mutable {
        var id: Int?
        var title: String
        var body: String
        var done: Bool
        var canceled: Bool
        var finished: Bool
        var errorMessage: String?
        var creatingPost: Bool
        
        static func initial(post: Post?) -> State {
            guard let post = post else {
                return empty
            }
            return State(
                id: post.id,
                title: post.title ?? "",
                body: post.body,
                done: false,
                canceled: false,
                finished: false,
                errorMessage: nil,
                creatingPost: false
            )
        }
        
        static var empty: State {
            return State(id: nil, title: "", body: "", done: false, canceled: false, finished: false, errorMessage: nil, creatingPost: false)
        }
        
        var createPost: CreatePostRequest? {
            if done {
                return CreatePostRequest(title: title, body: body)
            } else {
                return nil
            }
        }
    }
    
    
    
    enum Event {
        case none
        case titleDidChange(String)
        case bodyDidChange(String)
        case didCreatePost
        case createPost
        case cancel
        case didFail(String)
        case viewDidAppear
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .createPost:
            return state.mutate {
                $0.creatingPost = true
                $0.errorMessage = nil
                $0.done = true
                $0.finished = false
                $0.canceled = false
            }
        case .viewDidAppear:
            return state.mutate {
                $0.errorMessage = nil
                $0.done = false
                $0.finished = false
                $0.canceled = false                
            }
        case .didFail(let errorMessage):
            return state.mutate {
                $0.creatingPost = false
                $0.errorMessage = errorMessage
                $0.done = false
                $0.finished = false
                $0.canceled = false
            }
        case .cancel:
            return state.mutate {
                $0.done = false
                $0.finished = false
                $0.canceled = true
            }
        case .none:
            return state
        case .didCreatePost:
            return state.mutate {
                $0.done = false
                $0.finished = true
                $0.creatingPost = false
            }
        case .titleDidChange(let title):
            return state.mutate {
                $0.title = title
            }
        case .bodyDidChange(let body):
            return state.mutate {
                $0.body = body
            }
        }
    }
}
