//
//  PostCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import PostUI
import RxSwift


final class PostCoordinator: Coordinator<Link<User>> {
    
    private let viewController: PostViewController = PostViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    private let createPostHandler: CreatePostHandler
    var onCancel: (() -> Void)?
    var onFinished: (() -> Void)?
    var onDone: (() -> Void)?
    
    deinit {
        print("DEINIT POST CREATE")
    }
    
    init(
        router: RouterProtocol,
        createPostHandler: @escaping CreatePostHandler
    ) {
        self.createPostHandler = createPostHandler
        super.init(router: router)
    }
    
    
    override func start(with link: Link<User>?) {
        router.setRoot(viewController, animated: true)
        viewController.loadViewIfNeeded()
        
        bindPostViewController(viewController, post: nil, createPostHandler: createPostHandler)
            .do(onNext: { [unowned self] state in
                if state.canceled {
                    self.onCancel?()
                }
                if state.finished {
                    self.onFinished?()
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
}

