//
//  Services.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation
import RxSwift


protocol AppService: AnyObject {
    func loadUser() -> Single<Authentication>
    func signIn(email: String, password: String) -> Single<Authentication>
    func signUp(name: String, email: String, password: String, confirm: String) -> Single<Authentication>
    func getPosts() -> Single<[Post]>
    func createPost(body: String) -> Single<Post>
}





