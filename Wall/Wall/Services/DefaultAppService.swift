//
//  DefaultAppService.swift
//  Wall
//
//  Created by Mario Canto on 9/11/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation
import RxSwift

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}

typealias HTTPParameters = [String: Any]
typealias HTTPHeaders = [String: String]

struct EndPoint {
    let path: String
    let queryItems: [URLQueryItem]
}



extension EndPoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = Config.host
        components.path = path
        components.queryItems = queryItems
        
        return components.url
    }
}

//protocol Endpoint {
//    var baseURL: URL { get }
//    var path: String { get }
//    var httpMethod: HTTPMethod { get }
//    var headers: HTTPHeaders { get }
//}

enum API: String {
    case wallAndPost = "/posts"
}

final class DefaulAppService: AppService {
    
    
    private let urlSession = URLSession.shared
    
    func loadUser() -> Single<Authentication> {
        
        return Single<Authentication>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                if let authentication: Authentication = SessionStorage.valueFor(key: SessionStorage.Keys.authToken) {
                    single(.success(authentication))
                } else {
                    let error = NSError(
                        domain: "API",
                        code: 401,
                        userInfo: [NSLocalizedDescriptionKey : "Unauthorized"]
                    )
                    single(.error(error))
                }
            }
            return Disposables.create()
        }
        
    }
    
    func signUp(name: String, email: String, password: String, confirm: String) -> Single<Authentication> {
        let session = urlSession
        return Single<Authentication>.create { single in
            if name.isEmpty {
                let error = NSError(
                    domain: "App",
                    code: 1000,
                    userInfo: [NSLocalizedDescriptionKey : "Choose a valid name"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else if email.isEmpty || password.isEmpty  {
                let error = NSError(
                    domain: "API",
                    code: 401,
                    userInfo: [NSLocalizedDescriptionKey : "Email or Password is invalid"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else if !isValidEmail(email) {
                let error = NSError(
                    domain: "App",
                    code: 1001,
                    userInfo: [NSLocalizedDescriptionKey : "Invalid Email"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else if password != confirm {
                let error = NSError(
                    domain: "App",
                    code: 1002,
                    userInfo: [NSLocalizedDescriptionKey : "Passwords don't match"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else {
                let json: [String: Any] = [
                    "name" : name,
                    "email": email,
                    "password": password
                ]
                
                let jsonData = try! JSONSerialization.data(withJSONObject: json)
                
                let baseURL = URL(string: Config.basePath + "/signup")!
                var request = URLRequest(url: baseURL)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                request.httpMethod = "POST"
                request.httpBody = jsonData
                
                
                let dataTask = session.dataTask(with: request) { data, response, error in
                    if let data = data {
                        let decoder = JSONDecoder()
                        if let auth: Authentication = try? decoder.decode(Authentication.self, from: data) {
                            do {
                                try SessionStorage.save(auth, forKey: SessionStorage.Keys.authToken)
                                single(.success(auth))
                            } catch {
                                single(SingleEvent.error(error))
                            }
                        } else {
                            let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                            let info = error.flatMap({ $0.localizedDescription }) ?? "Try again later..."
                            let error = NSError(
                                domain: "API",
                                code: statusCode,
                                userInfo: [NSLocalizedDescriptionKey : info]
                            )
                            single(SingleEvent.error(error))
                        }
                    } else if let error = error {
                        single(SingleEvent.error(error))
                    } else {
                        let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                        let error = NSError(
                            domain: "API",
                            code: statusCode,
                            userInfo: [NSLocalizedDescriptionKey : "Try again later..."]
                        )
                        single(SingleEvent.error(error))
                    }
                }
                dataTask.resume()
                return Disposables.create {
                    dataTask.cancel()
                }
            }
        }
    }
    
    func signIn(email: String, password: String) -> Single<Authentication> {
        
        
        let json: [String: Any] = ["email": email,
                                   "password": password]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: json)
        
        let baseURL = URL(string: Config.basePath + "/auth/login")!
        var request = URLRequest(url: baseURL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        let session = urlSession
        
        return Single<Authentication>.create { single in
            
            if email.isEmpty || password.isEmpty {
                let error = NSError(
                    domain: "API",
                    code: 401,
                    userInfo: [NSLocalizedDescriptionKey : "Email or Password is invalid"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else if !isValidEmail(email) {
                let error = NSError(
                    domain: "App",
                    code: 1001,
                    userInfo: [NSLocalizedDescriptionKey : "Invalid Email"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            } else {
                let dataTask = session.dataTask(with: request) { data, response, error in
                    if let data = data {
                        let decoder = JSONDecoder()
                        if let auth: Authentication = try? decoder.decode(Authentication.self, from: data) {
                            do {
                                try SessionStorage.save(auth, forKey: SessionStorage.Keys.authToken)
                                single(.success(auth))
                            } catch {
                                single(SingleEvent.error(error))
                            }
                        } else {
                            let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                            let info = error.flatMap({ $0.localizedDescription }) ?? "Invalid credentials"
                            let error = NSError(
                                domain: "API",
                                code: statusCode,
                                userInfo: [NSLocalizedDescriptionKey : info]
                            )
                            single(SingleEvent.error(error))
                        }
                    } else if let error = error {
                        single(SingleEvent.error(error))
                    } else {
                        let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                        let error = NSError(
                            domain: "API",
                            code: statusCode,
                            userInfo: [NSLocalizedDescriptionKey : "Try again later..."]
                        )
                        single(SingleEvent.error(error))
                    }
                }
                dataTask.resume()
                return Disposables.create {
                    dataTask.cancel()
                }
            }
        }
    }
    
    func getPosts() -> Single<[Post]> {
        let session = urlSession
        return Single<[Post]>.create { single in
            
            guard let url = EndPoint(path: API.wallAndPost.rawValue, queryItems: []).url else {
                let error = NSError(
                    domain: "App",
                    code: 1001,
                    userInfo: [NSLocalizedDescriptionKey : "Invalid URL"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            }
            
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = HTTPMethod.get.rawValue
            
            let dataTask = session.dataTask(with: request) { data, response, error in
                if let data = data {
                    let decoder = JSONDecoder()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    do {
                        let posts: [Post] = try decoder.decode([Post].self, from: data)
                        single(.success(posts))
                    } catch {
                        single(SingleEvent.error(error))
                    }
                } else if let error = error {
                    single(SingleEvent.error(error))
                } else {
                    let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                    let error = NSError(
                        domain: "API",
                        code: statusCode,
                        userInfo: [NSLocalizedDescriptionKey : "Try again later..."]
                    )
                    single(SingleEvent.error(error))
                }
            }
            dataTask.resume()
            return Disposables.create {
                dataTask.cancel()
            }
        }
    }
    
    func createPost(body: String) -> Single<Post> {
        let session = urlSession
        return Single<Post>.create { single in
            
            guard !body.isEmpty else {
                let error = NSError(
                    domain: "App",
                    code: 1001,
                    userInfo: [NSLocalizedDescriptionKey : "Content can't be blank"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            }
            
            guard let url = EndPoint(path: API.wallAndPost.rawValue, queryItems: []).url else {
                let error = NSError(
                    domain: "App",
                    code: 1001,
                    userInfo: [NSLocalizedDescriptionKey : "Invalid URL"]
                )
                single(SingleEvent.error(error))
                return Disposables.create()
            }
            
            
            var request = URLRequest(url: url)
            do {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                if let auth: Authentication = SessionStorage.valueFor(key: SessionStorage.Keys.authToken) {
                    request.setValue(auth.authToken, forHTTPHeaderField: "Authorization")
                }
                request.httpMethod = HTTPMethod.post.rawValue
                let json: [String: Any] = ["content": body]
                let jsonData = try JSONSerialization.data(withJSONObject: json)
                request.httpBody = jsonData
            } catch {
                single(SingleEvent.error(error))
                return Disposables.create()
            }
            let dataTask = session.dataTask(with: request) { data, response, error in
                if let data = data {
                    let decoder = JSONDecoder()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    do {
                        let post: Post = try decoder.decode(Post.self, from: data)
                        single(.success(post))
                    } catch {
                        single(SingleEvent.error(error))
                    }
                } else if let error = error {
                    single(SingleEvent.error(error))
                } else {
                    let statusCode = response.flatMap({ ($0 as? HTTPURLResponse)?.statusCode }) ?? 0
                    let error = NSError(
                        domain: "API",
                        code: statusCode,
                        userInfo: [NSLocalizedDescriptionKey : "Try again later..."]
                    )
                    single(SingleEvent.error(error))
                }
            }
            dataTask.resume()
            return Disposables.create {
                dataTask.cancel()
            }
        }
    }
    
}
