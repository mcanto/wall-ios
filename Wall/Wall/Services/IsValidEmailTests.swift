//
//  IsValidEmailTests.swift
//  ServicesTests
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import XCTest
@testable import Wall

class IsValidEmailTests: XCTestCase {
    
    func testIsValidEmail() {
        XCTAssertFalse(isValidEmail("mario@me."))
        XCTAssertFalse(isValidEmail("mario.canto@me"))
        XCTAssertFalse(isValidEmail("m@mobile"))
        XCTAssertFalse(isValidEmail("@me.com"))
        XCTAssertFalse(isValidEmail("@."))
        XCTAssertTrue(isValidEmail("mario@me.com"))
        XCTAssertTrue(isValidEmail("MARIO.CANTO@me.com"))
        XCTAssertTrue(isValidEmail("mario+canto@gmail.com"))
        XCTAssertTrue(isValidEmail("a@b.c"))
        XCTAssertTrue(isValidEmail("mario@out.look.co"))
    }
    
}
