//
//  IsValidPassword.swift
//  Wall
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation

public func isValidPassword(_ password: String) -> Bool {
    
    // at least one uppercase,
    // at least one digit
    // at least one lowercase
    // 8 characters total
    let expression = "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
    let predicate = NSPredicate(format: "SELF MATCHES %@", expression)
    return predicate.evaluate(with: password)
    
    
    
//    guard password.count >= 8 else { return false }
//    guard password.count <= 15 else { return false }
//
//    guard password.rangeOfCharacter(from: NSCharacterSet.letters) != nil else { return false }
//    guard password.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil else { return false }
//    guard password.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines) == nil else { return false }
    
//        let capitalLetterRegEx  = ".*[A-Z]+.*"
//        let capitalLetterTest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
//        guard capitalLetterTest.evaluate(with: password) else { return false }
    
//    let capitalLetterRegEx  = ".*[A-Z,a-z]+.*"
//    let capitalLetterTest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
//    guard capitalLetterTest.evaluate(with: password) else { return false }
//
//    let numberRegEx  = ".*[0-9]+.*"
//    let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
//    guard numberTest.evaluate(with: password) else { return false }
    
    //    let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
    //    let specialCharacterTest = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
    //    guard specialCharacterTest.evaluate(with: password) else { return false }
    
//    return true
}
