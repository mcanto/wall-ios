//
//  IsValidPasswordTests.swift
//  ServicesTests
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import XCTest
@testable import Wall

class IsValidPasswordTests: XCTestCase {
    
    func testIsValidPassword() {
        XCTAssertFalse(isValidPassword("12345678"))
        XCTAssertFalse(isValidPassword("abcdefg"))
        XCTAssertFalse(isValidPassword("abcdefgljkadsljdaskjadsdsadasdsa"))
        XCTAssertFalse(isValidPassword("79391273912038901230921023"))
        XCTAssertFalse(isValidPassword("hidhsaiodhsoaidhisoa98127389173kjkhkjhkj"))
        XCTAssertFalse(isValidPassword("AJSAIUASDJ"))
        XCTAssertFalse(isValidPassword("asds123 s2"))
        XCTAssertFalse(isValidPassword("          1s"))
        XCTAssertFalse(isValidPassword("           "))
        
        XCTAssertFalse(isValidPassword("hadshaij12321"))
        
        XCTAssertFalse(isValidPassword("12345Ab"))
        XCTAssertFalse(isValidPassword("123456A"))
        XCTAssertFalse(isValidPassword("A123456"))
        XCTAssertFalse(isValidPassword("123456789012345A"))
        
        XCTAssertFalse(isValidPassword("12345678901234a"))
        
        XCTAssertTrue(isValidPassword("123456789012345Ab"))
        XCTAssertFalse(isValidPassword("12345678901234A"))
        XCTAssertTrue(isValidPassword("12345678Ab"))
        XCTAssertTrue(isValidPassword("Ab12345678"))
        
    }
    
}
