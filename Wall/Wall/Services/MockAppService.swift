//
//  MockAppService.swift
//  Wall
//
//  Created by Mario Canto on 9/11/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation
import RxSwift

final class MockAppService: AppService {
    
    func loadUser() -> Single<Authentication> {
        
        return Single<Authentication>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                if let authentication: Authentication = SessionStorage.valueFor(key: SessionStorage.Keys.authToken) {
                    single(.success(authentication))
                } else {
                    let error = NSError(
                        domain: "API",
                        code: 401,
                        userInfo: [NSLocalizedDescriptionKey : "Unauthorized"]
                    )
                    single(.error(error))
                }
            }
            return Disposables.create()
        }
        
    }
    
    func signUp(name: String, email: String, password: String, confirm: String) -> Single<Authentication> {
        return Single<Authentication>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                if name.isEmpty {
                    let error = NSError(
                        domain: "App",
                        code: 1000,
                        userInfo: [NSLocalizedDescriptionKey : "Choose a valid name"]
                    )
                    single(SingleEvent.error(error))
                } else if email.isEmpty || password.isEmpty  {
                    let error = NSError(
                        domain: "API",
                        code: 401,
                        userInfo: [NSLocalizedDescriptionKey : "Email or Password is invalid"]
                    )
                    single(SingleEvent.error(error))
                } else if !isValidEmail(email) {
                    let error = NSError(
                        domain: "App",
                        code: 1001,
                        userInfo: [NSLocalizedDescriptionKey : "Invalid Email"]
                    )
                    single(SingleEvent.error(error))
                } else if password != confirm {
                    let error = NSError(
                        domain: "App",
                        code: 1002,
                        userInfo: [NSLocalizedDescriptionKey : "Passwords don't match"]
                    )
                    single(SingleEvent.error(error))
                } else {
                    let authToken = Authentication(authToken: UUID().uuidString)
                    try? SessionStorage.save(authToken, forKey: SessionStorage.Keys.authToken)
                    single(SingleEvent.success(authToken))
                }
            }
            return Disposables.create()
        }
    }
    
    func signIn(email: String, password: String) -> Single<Authentication> {
        return Single<Authentication>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                if email.isEmpty || password.isEmpty {
                    let error = NSError(
                        domain: "API",
                        code: 401,
                        userInfo: [NSLocalizedDescriptionKey : "Email or Password is invalid"]
                    )
                    single(SingleEvent.error(error))
                } else if !isValidEmail(email) {
                    let error = NSError(
                        domain: "App",
                        code: 1001,
                        userInfo: [NSLocalizedDescriptionKey : "Invalid Email"]
                    )
                    single(SingleEvent.error(error))
                } else {
                    let authToken = Authentication(authToken: UUID().uuidString)
                    try? SessionStorage.save(authToken, forKey: SessionStorage.Keys.authToken)
                    single(SingleEvent.success(authToken))                    
                }
            }
            return Disposables.create()
        }
    }
    
    func getPosts() -> Single<[Post]> {
        return Single<[Post]>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                
                let posts = (0...10).map({
                    Post(id: $0, author: Author(id: $0, name: "unamed", email: "unamed@me.com"))
                })
                single(SingleEvent.success(posts))
            }
            return Disposables.create()
        }
    }
    
    func createPost(body: String) -> Single<Post> {
        return Single<Post>.create { single in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                single(SingleEvent.success(Post(
                    id: Int.random(in: 0 ... 100),
                    author: Author(
                        id: Int.random(in: 0 ... 10),
                        name: "unamed",
                        email: "unamed@me.com"))))
            }
            return Disposables.create()
        }
    }
}
