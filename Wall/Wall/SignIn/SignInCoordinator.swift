//
//  SignInCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import SignInUI
import RxSwift


final class SignInCoordinator: Coordinator<Link<User>> {
    
    private let viewController: SignInViewController = SignInViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    private let signInHandler: SignInHandler
    var onSignedIn: ((Authentication) -> Void)?
    var onHoldSigIn: (() -> Void)?
    var onSignUp: (() -> Void)?
    
    deinit {
        print("SIGN IN DEINIT")
    }
    
    init(
        router: RouterProtocol,
        signInHandler: @escaping SignInHandler        
    ) {
        self.signInHandler = signInHandler
        super.init(router: router)        
    }
    
    override func start(with link: Link<User>?) {
        viewController.loadViewIfNeeded()
        bindSignInViewController(viewController, signInHandler: signInHandler)
            .debug("Sign in", trimOutput: false)
            .do(onNext: { [unowned self] state in
                if state.onHoldSignIn {
                    self.onHoldSigIn?()
                }
                if state.signUp {
                    self.onSignUp?()
                }
                if let user = state.user {
                    self.onSignedIn?(user)
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
    
    override var presentable: UIViewController {
        return viewController
    }
}
