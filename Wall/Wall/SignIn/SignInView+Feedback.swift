//
//  SignInView+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import SignInUI

typealias SignInViewFeedback = (ObservableSchedulerContext<SignInView.State>) -> Observable<SignInView.Event>
typealias SignInHandler = (String, String) -> Single<Authentication>

func bindSignInViewController(
    _ viewController: SignInViewController,
    signInHandler: @escaping SignInHandler)
    -> Observable<SignInView.State> {
        
        var uiBindings: SignInViewFeedback {
            
            return bind(viewController) { (me, state) -> Bindings<SignInView.Event> in
                
                
                
                
                let subscriptions: [Disposable] = [
                    state.map({ $0.email }).bind(to: viewController.emailTextField.rx.text),
                    state.map({ $0.password }).bind(to: viewController.passwordTextField.rx.text),
                    state.map({ $0.isSigningIn }).bind(to: viewController.activityIndicatorView.rx.isAnimating),
                    state.map({ !$0.isSigningIn }).bind(to: viewController.submitButton.rx.isEnabled),
                    state.map({ !$0.isSigningIn }).bind(to: viewController.signUpButton.rx.isEnabled),
                    state.map({ !$0.isSigningIn }).bind(to: viewController.useWallButton.rx.isEnabled),
                    state.map({ !$0.isSigningIn }).bind(to: viewController.emailTextField.rx.isEnabled),
                    state.map({ !$0.isSigningIn }).bind(to: viewController.passwordTextField.rx.isEnabled),
                    state.map({ $0.errorMessage }).filter({ $0 != nil }).subscribe(onNext: { message in
                        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        viewController.present(alertController, animated: true, completion: nil)
                    })
                ]
                
                let mutations: [Observable<SignInView.Event>] = [
                    viewController.emailTextField.rx.text.orEmpty.map(SignInView.Event.emailDidChange),
                    viewController.passwordTextField.rx.text.orEmpty.map(SignInView.Event.passwordDidChange),
                    viewController.useWallButton.rx.tap.throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in SignInView.Event.onHoldSignIn }),
                    viewController.signUpButton.rx.tap.throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in SignInView.Event.signUp }),
                    viewController.submitButton.rx.tap.throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in SignInView.Event.signIn }),
                    viewController.rx.viewDidDisappear.map({ _ in SignInView.Event.viewDidDissapear })
                ]
                return Bindings(subscriptions: subscriptions, events: mutations)
                
            }
        }
        
        
        let signInFeedback: SignInViewFeedback = react(request: { $0.signIn }) { request -> Observable<SignInView.Event> in
            return signInHandler(request.email, request.password)
                .debug("sign in handler", trimOutput: false)
                .map(SignInView.Event.success)
                .catchError({ error in
                    Single.just(SignInView.Event.failure(error.localizedDescription))                    
                })
                .asObservable()
        }
        
        
        return Observable<Any>.system(
            initialState: SignInView.State.empty,
            reduce: SignInView.reduce,
            scheduler: MainScheduler.instance,
            feedback: uiBindings, signInFeedback
        ).share()
        
}
