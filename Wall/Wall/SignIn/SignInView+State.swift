//
//  SignInView+State.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//


struct SignInRequest: Equatable {
    let email: String
    let password: String
}

struct SignInView {
    struct State: Mutable {
        var email: String
        var password: String
        var isSigningIn: Bool
        var user: Authentication?
        var onHoldSignIn: Bool
        var signUp: Bool
        var errorMessage: String?
        
        static var empty: State {
            return State(
                email: "",
                password: "",
                isSigningIn: false,
                user: nil,
                onHoldSignIn: false,
                signUp: false,
                errorMessage: nil
            )
        }
        
        var signIn: SignInRequest? {
            if isSigningIn {
                return SignInRequest(email: email, password: password)
            } else {
                return nil
            }
        }
    }
    enum Event {
        case emailDidChange(String)
        case passwordDidChange(String)
        case viewDidDissapear
        case onHoldSignIn
        case signIn
        case signUp
        case success(Authentication)
        case failure(String)
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .emailDidChange(let email):
            return state.mutate {
                $0.email = email
                $0.errorMessage = nil
            }
        case .passwordDidChange(let password):
            return state.mutate {
                $0.password = password
                $0.errorMessage = nil
            }
        case .signUp:
            return state.mutate {
                $0.isSigningIn = false
                $0.user = nil
                $0.email = ""
                $0.password = ""
                $0.errorMessage = nil
                $0.onHoldSignIn = false
                $0.signUp = true
            }
        case .viewDidDissapear:
            return state.mutate {
                $0.isSigningIn = false
                $0.user = nil
                $0.email = ""
                $0.password = ""
                $0.errorMessage = nil
                $0.onHoldSignIn = false
                $0.signUp = false
            }
        case .onHoldSignIn:
            return state.mutate {
                $0.isSigningIn = false
                $0.user = nil
                $0.email = ""
                $0.password = ""
                $0.errorMessage = nil
                $0.onHoldSignIn = true
                $0.signUp = false
            }
        case .signIn:
            return state.mutate {
                $0.isSigningIn = true
                $0.errorMessage = nil
            }
        case .success(let authentication):
            return state.mutate {
                $0.isSigningIn = false
                $0.user = authentication
                $0.email = ""
                $0.password = ""
                $0.errorMessage = nil
                $0.signUp = false
            }
        case .failure(let message):
            return state.mutate {
                $0.isSigningIn = false
                $0.user = nil
                $0.email = ""
                $0.password = ""
                $0.errorMessage = message
                $0.signUp = false
            }
        
        }
    }
}
