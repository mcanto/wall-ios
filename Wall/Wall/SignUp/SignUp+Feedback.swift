//
//  SignUp+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import SignUpUI


typealias SignUpViewFeedback = (ObservableSchedulerContext<SignUpView.State>) -> Observable<SignUpView.Event>
typealias SignUpHandler = (String, String, String, String) -> Single<Authentication>

func bindSignUpViewController(
    _ viewController: SignUpViewController,
    signUpHandler: @escaping SignUpHandler)
-> Observable<SignUpView.State> {
        
        var uiBindings: SignUpViewFeedback {
            
            return bind(viewController) { (me, state) -> Bindings<SignUpView.Event> in
                
                
                
                
                let subscriptions: [Disposable] = [
                    state.map({ $0.name }).bind(to: viewController.nameTextField.rx.text),
                    state.map({ $0.email }).bind(to: viewController.emailTextField.rx.text),
                    state.map({ $0.password }).bind(to: viewController.passwordTextField.rx.text),
                    state.map({ $0.passwordConfirmation }).bind(to: viewController.passwordConfirmationTextField.rx.text),
                    state.map({ $0.isSigningUp }).bind(to: viewController.activityIndicatorView.rx.isAnimating),
                    state.map({ !$0.isSigningUp }).bind(to: viewController.submitButton.rx.isEnabled),
                    state.map({ !$0.isSigningUp }).bind(to: viewController.nameTextField.rx.isEnabled),
                    state.map({ !$0.isSigningUp }).bind(to: viewController.emailTextField.rx.isEnabled),
                    state.map({ !$0.isSigningUp }).bind(to: viewController.passwordTextField.rx.isEnabled),
                    state.map({ !$0.isSigningUp }).bind(to: viewController.passwordConfirmationTextField.rx.isEnabled),
                    state.map({ $0.errorMessage }).filter({ $0 != nil }).subscribe(onNext: { message in
                        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        viewController.present(alertController, animated: true, completion: nil)
                    })
                ]
                
                let mutations: [Observable<SignUpView.Event>] = [
                    viewController.nameTextField.rx.text.orEmpty.map(SignUpView.Event.nameDidChange),
                    viewController.emailTextField.rx.text.orEmpty.map(SignUpView.Event.emailDidChange),
                    viewController.passwordTextField.rx.text.orEmpty.map(SignUpView.Event.passwordDidChange),
                    viewController.passwordConfirmationTextField.rx.text.orEmpty.map(SignUpView.Event.passwordConfirmationDidChange),
                    viewController.submitButton.rx.tap.throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in SignUpView.Event.signUp }),
                    viewController.rx.viewDidDisappear.map({ _ in SignUpView.Event.viewDidDissapear })
                ]
                return Bindings(subscriptions: subscriptions, events: mutations)
                
            }
        }
        
        
        let signUpFeedback: SignUpViewFeedback = react(request: { $0.signUp }) { request -> Observable<SignUpView.Event> in
            return signUpHandler(request.name, request.email, request.password, request.passwordConfirmation)
                .map(SignUpView.Event.success)
                .catchError({ error in Single.just(SignUpView.Event.failure(error.localizedDescription)) })
                .asObservable()
        }
        
        
        return Observable<Any>.system(
            initialState: SignUpView.State.empty,
            reduce: SignUpView.reduce,
            scheduler: MainScheduler.instance,
            feedback: uiBindings, signUpFeedback
        ).share()
        
}
