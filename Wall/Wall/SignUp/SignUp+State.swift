//
//  SignUp+State.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct SignUpRequest: Equatable {
    let name: String
    let email: String
    let password: String
    let passwordConfirmation: String
}

struct SignUpView {
    struct State: Mutable {
        var name: String
        var email: String
        var password: String
        var passwordConfirmation: String
        var isSigningUp: Bool
        var user: Authentication?
        var errorMessage: String?
        
        static var empty: State {
            return State(
                name: "",
                email: "",
                password: "",
                passwordConfirmation: "",
                isSigningUp: false,
                user: nil,
                errorMessage: nil
            )
        }
        
        var signUp: SignUpRequest? {
            if isSigningUp {
                return SignUpRequest(name: name, email: email, password: password, passwordConfirmation: passwordConfirmation)
            } else {
                return nil
            }
        }
    }
    enum Event {
        case nameDidChange(String)
        case emailDidChange(String)
        case passwordDidChange(String)
        case passwordConfirmationDidChange(String)
        case viewDidDissapear        
        case signUp
        case success(Authentication)
        case failure(String)
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .nameDidChange(let name):
            return state.mutate {
                $0.name = name
                $0.errorMessage = nil
            }
        case .emailDidChange(let email):
            return state.mutate {
                $0.email = email
                $0.errorMessage = nil
            }
        case .passwordDidChange(let password):
            return state.mutate {
                $0.password = password
                $0.errorMessage = nil
            }
        case .passwordConfirmationDidChange(let password):
            return state.mutate {
                $0.passwordConfirmation = password
                $0.errorMessage = nil
            }
        case .viewDidDissapear:
            return state.mutate {
                $0.isSigningUp = false
                $0.user = nil
                $0.name = ""
                $0.email = ""
                $0.password = ""
                $0.errorMessage = nil
            }
        case .signUp:
            return state.mutate {
                $0.isSigningUp = true
                $0.errorMessage = nil
            }
        case .success(let authentication):
            return state.mutate {
                $0.isSigningUp = false
                $0.user = authentication
                $0.name = ""
                $0.email = ""
                $0.password = ""
                $0.passwordConfirmation = ""
                $0.errorMessage = nil
            }
        case .failure(let message):
            return state.mutate {
                $0.isSigningUp = false
                $0.user = nil                
                $0.password = ""
                $0.passwordConfirmation = ""
                $0.errorMessage = message
            }
            
        }
    }
}

