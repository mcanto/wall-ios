//
//  SignUpCoordinator.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import SignUpUI
import RxSwift


final class SignUpCoordinator: Coordinator<Link<User>> {
    
    private let viewController: SignUpViewController = SignUpViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    private let signUpHandler: SignUpHandler
    
    var onSignedUp: ((Authentication) -> Void)?
    
    deinit {
        print("DEINIT SIGN UP")
    }
    
    init(
        router: RouterProtocol,
        signUpHandler: @escaping SignUpHandler
    ) {
        self.signUpHandler = signUpHandler
        super.init(router: router)
    }
    
    override var presentable: UIViewController {
        return viewController
    }
    
    override func start(with link: Link<User>?) {
        viewController.loadViewIfNeeded()
        bindSignUpViewController(viewController, signUpHandler: signUpHandler)
            .debug("Sign Up", trimOutput: false)
            .do(onNext: { [unowned self] state in
                if let authentication = state.user {
                    self.onSignedUp?(authentication)
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
}


