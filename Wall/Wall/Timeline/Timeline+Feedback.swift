//
//  Timeline+Feedback.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import RxSwift
import RxCocoa
import RxFeedback
import TimelineUI

typealias TimelineViewFeedback = (ObservableSchedulerContext<TimelineView.State>) -> Observable<TimelineView.Event>
typealias GetPostsHandler = () -> Single<[Post]>


func bindTimelineViewController(
    _ viewController: TimelineViewController,
    authToken: String?,
    getPostsHandler: @escaping GetPostsHandler)
-> Observable<TimelineView.State> {
        
        var uiBindings: TimelineViewFeedback {
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            
            return bind(viewController) { (me, state) -> Bindings<TimelineView.Event> in
                
                let cellHandler: (Int, Post, TimelineTableCell) -> Void = { index, post, cell in
                    cell.bodyLabel.text = post.body
                    cell.usernameLabel.text = post.author.name
                    cell.createdAtLabel.text = dateFormatter.string(from: post.createdAt)
                }
                
                let subscriptions: [Disposable] = [
                    state.map({ $0.authToken != nil }).bind(to: viewController.rx.isLoggedIn),
                    state.map({ $0.posts })
                        .bind(to: me.tableView.rx.items(cellIdentifier: String(describing: TimelineTableCell.self), cellType: TimelineTableCell.self))(cellHandler)
                ]
                
                let mutations: [Observable<TimelineView.Event>] = [
                    NotificationCenter.default.rx.notification(DidCancelPostCreationNotificationName)
                        .share()
                        .map({_ in TimelineView.Event.viewDidDisappear }),
                    me.tableView.rx.modelSelected(Post.self).map(TimelineView.Event.didSelectModel),
                    me.rx.viewDidAppear.map({ _ in TimelineView.Event.getPosts }),
                    me.rx.viewDidDisappear.map({ _ in TimelineView.Event.viewDidDisappear }),
                    me.logOutBarButtonItem.rx.tap
                        .throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in TimelineView.Event.logOut }),
                    viewController.postBarButtonItem.rx.tap                        
                        .throttle(DispatchTimeInterval.milliseconds(3), scheduler: MainScheduler.instance)
                        .map({ _ in TimelineView.Event.createPost }),                    
                ]
                return Bindings(subscriptions: subscriptions, events: mutations)
                
            }
        }
        
        
        let getPostsFeedback: TimelineViewFeedback = react(request: { $0.getPostsRequest }) { _ -> Observable<TimelineView.Event> in
            return getPostsHandler()
                .map(TimelineView.Event.didGetPosts)
                .catchError({  Single.just(TimelineView.Event.didFail($0.localizedDescription)) })
                .asObservable()
        }
        
        
        return Observable<Any>.system(
            initialState: TimelineView.State(authToken: authToken),
            reduce: TimelineView.reduce,
            scheduler: MainScheduler.instance,
            feedback: uiBindings, getPostsFeedback
            ).share()
        
}

extension Reactive where Base: TimelineViewController {
    
    var isLoggedIn: Binder<Bool> {
        
        return Binder(base) { target, isLoggedIn in
            
            if isLoggedIn {                
                self.base.navigationItem.leftBarButtonItems = [self.base.logOutBarButtonItem]
            } else {
                self.base.navigationItem.leftBarButtonItems = []
            }
        }
    }
    
}
