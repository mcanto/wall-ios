//
//  Timeline+State.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

struct GetPostsRequest: Equatable {}



struct TimelineView {
    enum Selection {
        case none
        case model(Post)
    }
    
    struct State: Mutable {
        var posts: [Post]
        var createPost: Bool
        var getPosts: Bool
        var authToken: String?
        var errorMessage: String?
        var selection: Selection
        var logOut: Bool
        
        init(posts: [Post] = [], createPost: Bool = false, getPosts: Bool = false, authToken: String?, errorMessage: String? = nil, selection: Selection = .none, logOut: Bool = false) {
            self.posts = posts
            self.createPost = createPost
            self.getPosts = getPosts
            self.authToken = authToken
            self.errorMessage = errorMessage
            self.selection = selection
            self.logOut = logOut
        }
        
        
        static var empty: State {
            return State(posts: [], createPost: false, getPosts: true, authToken: nil, errorMessage: nil)
        }
        
        var getPostsRequest: GetPostsRequest? {
            if getPosts {
                return GetPostsRequest()
            } else {
                return nil
            }
        }
    }
    enum Event {
        case none
        case getPosts
        case didGetPosts([Post])
        case createPost
        case viewDidAppear
        case viewDidDisappear
        case didFail(String)
        case didSelectModel(Post)
        case logOut
    }
    
    static func reduce(_ state: State, event: Event) -> State {
        switch event {
        case .logOut:
            return state.mutate {
                $0.selection = .none
                $0.createPost = false
                $0.logOut = true
            }
        case .didSelectModel(let post):
            return state.mutate {
                $0.selection = .model(post)
            }
        case .didFail(let errorMessage):
            return state.mutate {
                $0.createPost = false                
                $0.errorMessage = errorMessage
                $0.selection = .none
            }
        case .viewDidDisappear:
            return state.mutate {
                $0.createPost = false
                $0.selection = .none
            }
        case .viewDidAppear:
            return state.mutate {
                $0.createPost = false
                $0.selection = .none
            }
        case .createPost:
            return state.mutate {
                $0.createPost = true
                $0.selection = .none
            }
        case .getPosts:
            return state.mutate {
                $0.getPosts = true
                $0.selection = .none
            }
        case .didGetPosts(let posts):
            return state.mutate {
                $0.getPosts = false
                $0.posts = posts
                $0.selection = .none
            }
        case .none:
            return state
        }
    }
}

