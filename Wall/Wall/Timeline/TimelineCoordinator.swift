//
//  TimelineViewController.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import NavigationKit
import TimelineUI
import RxSwift


let DidCancelPostCreationNotificationName = NSNotification.Name("cancel.not.logged.in.cannot.create.post")
final class TimelineCoordinator: Coordinator<Link<User>> {
    
    private let viewController: TimelineViewController = TimelineViewController.instantiate()!
    private let bag: DisposeBag = DisposeBag()
    private let getPostsHandler: GetPostsHandler
    
    var onCreatePost: ( () -> Void )?
    var onSignIn: (() -> Void)?
    var onPostSelection: ((Post) -> Void)?
    var onLogOut: (() -> Void)?
    private let authToken: String?
    
    deinit {
        print("DEINIT TIMELINE")
    }
    
    init(
        router: RouterProtocol,
        authToken: String?,
        getPostsHandler: @escaping GetPostsHandler
    ) {
        self.getPostsHandler = getPostsHandler
        self.authToken = authToken
        super.init(router: router)
    }
    
    
    override func start(with link: Link<User>?) {
        bindTimelineViewController(viewController, authToken: authToken, getPostsHandler: getPostsHandler)
            .do(onNext: { [unowned self] state in
                if self.authToken == nil && state.createPost {
                    let alertController = UIAlertController(
                        title: nil,
                        message: NSLocalizedString("Sign in to post", comment: ""),
                        preferredStyle: .alert)
                    let signInAction = UIAlertAction(title: "Sign In", style: .default) { _ in
                        self.onSignIn?()
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        NotificationCenter.default.post(name: DidCancelPostCreationNotificationName, object: self)
                    }
                    alertController.addAction(signInAction)
                    alertController.addAction(cancelAction)
                    self.router.present(alertController, animated: true, completion: nil)
                    
                } else if state.createPost {
                    self.onCreatePost?()
                } else if case TimelineView.Selection.model(let post) = state.selection {
                    self.onPostSelection?(post)
                } else if state.logOut {
                    self.onLogOut?()
                }
                
            })
            .subscribe()
            .disposed(by: bag)
    }
    
    
    override var presentable: UIViewController {
        return viewController
    }
}


