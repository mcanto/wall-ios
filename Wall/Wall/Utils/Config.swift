//
//  Config.swift
//  Wall
//
//  Created by Mario Canto on 9/11/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation

final class Config {
    static func infoForKey(_ key: String) -> String? {
        return (Bundle(for: Config.self).infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "")
    }
    
    static var basePath: String {
        return infoForKey("BasePath")!
    }
    
    static var host: String {
        return infoForKey("Host")!
    }
    
    static var bundleID: String {
        return infoForKey("CFBundleIdentifier")!
    }
    
}
