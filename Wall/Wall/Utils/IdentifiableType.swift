//
//  IdentifiableType.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation

//struct Identifier<Value>: Hashable {
//    let string: String
//}
//
//extension Identifier: ExpressibleByStringLiteral {
//    init(stringLiteral value: String) {
//        string = value
//    }
//}
//
//extension Identifier: CustomStringConvertible {
//    var description: String {
//        return string
//    }
//}
//
//extension Identifier: Codable {
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        string = try container.decode(String.self)
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encode(string)
//    }
//}

protocol Identifiable {
    associatedtype RawIdentifier: Codable = String
    
    var id: Identifier<Self> { get }
}

struct Identifier<Value: Identifiable> {
    let rawValue: Value.RawIdentifier
    
    init(rawValue: Value.RawIdentifier) {
        self.rawValue = rawValue
    }
}

extension Identifier: ExpressibleByIntegerLiteral
where Value.RawIdentifier == Int {
    typealias IntegerLiteralType = Int
    
    init(integerLiteral value: Int) {
        rawValue = value
    }
}

extension Identifier: Codable {}
