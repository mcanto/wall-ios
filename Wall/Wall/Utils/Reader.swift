//
//  Reader.swift
//  Wall
//
//  Created by Mario Canto on 9/9/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

final class Reader<E, A> {
    typealias Apply = (E) -> A
    private let g: Apply
    
    init(_ g: @escaping Apply) {
        self.g = g
    }
    
    func apply(_ e: E) -> A {
        return g(e)
    }
}

extension Reader {
    func map<B>(_ transform: @escaping (A) -> B) -> Reader<E, B> {
        return Reader<E, B> { e in
            transform(self.g(e))
        }
    }
    
    func flatMap<B>(_ transform: @escaping (A) -> Reader<E, B>) -> Reader<E, B> {
        return Reader<E, B> { e in
            transform(self.g(e)).g(e)
        }
    }
}

