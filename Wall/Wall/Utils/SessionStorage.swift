//
//  Storage.swift
//  Wall
//
//  Created by Mario Canto on 9/10/19.
//  Copyright © 2019 Mario Canto. All rights reserved.
//

import Foundation
import RxSwift

enum SessionStorageError: Error {
    case notFound
    case failedDecoding
    case failedEncoding
}

struct SessionStorage {
    
    struct Keys {
        static let currentUser = "com.wall.current.user"
        static let authToken = "com.wall.current.auth.token"
    }
    
    static private let encoder = JSONEncoder()
    static private let decoder = JSONDecoder()
    
    
    static func save<T: Encodable>(_ entity: T, forKey key: String) throws {
        let encoded = try encoder.encode(entity)
        let defaults = UserDefaults.standard
        defaults.set(encoded, forKey: key)
    }
    
    static func valueFor<T: Decodable>(key: String)  -> T? {
        let defaults = UserDefaults.standard
        guard let encoded = defaults.value(forKey: key) as? Data else {
            return nil
        }
        return try? decoder.decode(T.self, from: encoded)
    }
    
    static func removeAuth() {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: Keys.authToken)
    }
    
    static func setAuth<T: Encodable>(_ entity: T, forKey key: String) -> Observable<T> {
        
        return Observable<T>.create { observer in
            guard let encoded = try? encoder.encode(entity) else {
                observer.onError(SessionStorageError.failedEncoding)
                return Disposables.create()
            }
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key)
            observer.onNext(entity)
            
            return Disposables.create()
        }
    }

}
